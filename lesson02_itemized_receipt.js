window.addEventListener('load',function(){
    document.getElementById('btnAddObject').addEventListener("click", createItem);
    document.getElementById('btnGetSum').addEventListener("click", getSum);
});



// Item object
const Item = function(description,price) {
    this.description=description;
    this.price=price;
}

// Array of items
const Items = function () {
    this.items = [];

    this.addItem = function (item) {
        this.items.push(item);

        let node = document.createElement("li");
        let tnode = document.createTextNode(`Item: ${item.description}  Cost: ${item.price}`);
        node.appendChild(tnode)

        document.getElementById('itemList').appendChild(node);
    }

    this.getSum = function () {
        let acc=0;
        for(var i=0;i<this.items.length;i++){
            acc += parseFloat( this.items[i].price);
        }
        return acc;
    }

}

const createItem=function () {

    let item= new Item(document.getElementById('desc').value,document.getElementById('cost').value);
    items.addItem(item);
}

const getSum=function () {
    document.getElementById('total').innerHTML=items.getSum();

}


// Hold an array of items
var items = new Items();





