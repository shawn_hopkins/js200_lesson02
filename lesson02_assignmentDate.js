const getDueDate=function () {

    let dt=document.getElementById('dt').value;
    let _dt=dt.split('-');
    let days = parseInt( document.getElementById('days').value);

    let start = new Date(parseInt(_dt[0]),parseInt(_dt[1])-1,parseInt(_dt[2]));
    let enddt = new Date(start.getTime()+(days *24*60*60*1000));

    let html = document.getElementById('result');

    var options = {month:'long',day:'numeric',year:'numeric'};

    let tag='<br /><span>';
    tag +=`<span>${start.toLocaleDateString('en-US',options)}&nbsp;+&nbsp;${days.toString()}&nbsp;days&nbsp;`;
    tag +=`=&nbsp;</span><time datetime="${dt}">${enddt.toLocaleDateString('en-US',options)}</time>`;
    tag +='</span>';
    html.innerHTML += tag;

}
