window.addEventListener('load',function(){
    document.getElementById('btnAddShip').addEventListener("click", createShip);
});


// Array of ships
let ships = [];

// Spaceship object constructer
const Spaceship = function(name,speed) {
    let shipName=name;
    let topSpeed=speed;

    // name getter
    this.getName = function () {
        return shipName;
    }
    // Speed getter
    this.getSpeed = function () {
        return topSpeed;
    }
    // speed setter
    this.setSpeed=function (speed) {
        topSpeed=speed;
    }

    // add the ship to the ship table in the UI
    this.registerShip = function () {

        let tbl = document.getElementById('shipTable').getElementsByTagName('tbody')[0];
        let row  = tbl.insertRow(tbl.rows.length);

        let id = tbl.rows.length-2;
        // Name cell
        let cell0=row.insertCell(0);
        let val0=document.createTextNode(this.getName());
        cell0.appendChild(val0);

        // speed input
        let cell1=row.insertCell(1);
        let in1 = document.createElement('input');
        in1.setAttribute('id','i_'+id);
        in1.setAttribute('type','number');
        in1.setAttribute('readonly','true');
        in1.setAttribute('value',this.getSpeed());
        cell1.appendChild(in1);

        // allow user to modify speed
        let cell2=row.insertCell(2);
        cell2.setAttribute('class','btn-group');

        let b1 = document.createElement('button');
        b1.setAttribute('id','c_'+id);
        b1.setAttribute('class','btn btn-success tblBtn');
        b1.setAttribute('onclick','accelerate(this.id)');

        let i1 = document.createElement('i');
        i1.setAttribute('class','fa fa-edit');
        b1.appendChild(i1);
        cell2.appendChild(b1);

        // call function to display ship private attributes
        let b2 = document.createElement('button');
        b2.setAttribute('id','d_'+id);
        b2.setAttribute('class','btn btn-secondary tblBtn');
        b2.setAttribute('onclick','viewShip(this.id)');
        let i2 = document.createElement('i');
        i2.setAttribute('class','fa fa-fighter-jet');
        b2.appendChild(i2);
        cell2.appendChild(b2);
    }

    // Display the private attributes
    this.accelerate = function() {
        alert(`Ship '${this.getName()}' goes like ${this.getSpeed()} parsecs/s`);
    }
}


// Create new new instance of ship
const createShip=function () {

    let n = document.getElementById('shipName').value;
    let s = parseInt( document.getElementById('speed').value);

    if (n.length >0 && s > 0) {
        let ship = new Spaceship(document.getElementById('shipName').value,
                                parseInt(document.getElementById('speed').value));

        // Update the UI table the the ship details
        ship.registerShip();

        // Add to the ship array
        ships.push(ship);

        document.getElementById('shipName').value = '';
        document.getElementById('speed').value = '';
    }
}


// modify ui to accept a new speed for a ship
const accelerate =function (id) {
    let idx=parseInt(id.split('_')[1]);

    let speed = document.getElementById('i_'+idx);
    speed.readOnly=false;

    let btn = document.getElementById('c_'+idx);
    btn.setAttribute('class','btn btn-primary tblBtn');
    btn.removeChild(btn.childNodes[0]);

    let i1 = document.createElement('i');
    i1.setAttribute('class','fa fa-floppy-o');
    btn.appendChild(i1);
    btn.setAttribute('onclick','updateSpeed(this.id)');
    
}

// Enables UI to accept new speed and updates private attribute
const updateSpeed = function (id) {

    let idx=parseInt(id.split('_')[1]);

    let speed = document.getElementById('i_'+idx);
    speed.readOnly=true;

    let btn = document.getElementById('c_'+idx);
    btn.setAttribute('class','btn btn-success tblBtn');
    btn.setAttribute('onclick','accelerate(this.id)');
    btn.removeChild(btn.childNodes[0]);

    let i1 = document.createElement('i');
    i1.setAttribute('class','fa fa-edit');
    btn.appendChild(i1);

    // private attribute udated usign setter
    ships[idx].setSpeed(speed.value);
}

// Identify the correct ship and call the accerate method to view private attributes
const viewShip = function (id) {

    let idx=parseInt(id.split('_')[1]);
    ships[idx].accelerate();
}