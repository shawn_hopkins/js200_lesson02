
// create 2 players
const init = function() {
    player1 = new Player(20, 'player1','Player 1', 1);
    player2 = new Player(20, 'player2','Player 2', 1);
    document.getElementById('player1Label').innerHTML = player1.name;
    document.getElementById('player2Label').innerHTML = player2.name;
}

const Player = function(health,id,name,rank) {
    // Properties
    this.health = health;
    this.id=id;
    this.name=name;
    this.baseDamage = rank;

    // update the opactity of the opponents life indicator
    this.changeLife = function (player,damage) {

        let hit = parseInt(player.health) * .1;
        document.getElementById(player.id).style.opacity=hit;
        let health = document.getElementById(player.id + 'Life');
        let meter = document.getElementById(player.id + 'Health');
        health.value = player.health;
        meter.innerHTML = ((player.health / 20)*100).toString() + '%';

        // Identify the winner
        if (player.health <= 0){
            let winner = document.getElementById(player.id == player1.id ? 'player2Label' : 'player1Label');
            this.resetClass();
            winner.innerHTML += '<strong style="color:green;">&nbsp;!!!WINNER!!!</strong>';
            document.getElementById('player1Attack').disabled=true;
            document.getElementById('player2Attack').disabled=true;
            document.getElementById('replay').style.visibility ='visible';
        }
    }

    // Calculate the hit damage
    this.attack = function (victim) {

        let damage = this.baseDamage + Math.floor(Math.random() * 10);
        damage = damage > victim.health ? victim.health : damage;
        victim.health -= damage;

        this.trashTalk(damage);
        this.changeLife(victim)
    }

    // talk junk
    this.trashTalk= function(hit){
        let container = document.getElementById('trash');
        let msg = document.getElementById('trashMsg');

        this.resetClass();

        if (hit <3){
            container.classList.add('alert-warning');
            msg.innerHTML=`Weak hit, minus ${hit} health`;
        }
        else if (hit < 7){
            container.classList.add('alert-info');
            msg.innerHTML=`Solid hit, minus ${hit} health`;
        }
        else {
            container.classList.add('alert-danger');
            msg.innerHTML=`Savage hit, minus ${hit} health`;
        }
    }

    // restart the game
    this.resetClass = function () {

        let container = document.getElementById('trash');
        let classes=['alert-info','alert-warning','alert-danger'];
        for (i=0;i<classes.length;i++){
            container.classList.remove(classes[i]);
        }
    }
}

